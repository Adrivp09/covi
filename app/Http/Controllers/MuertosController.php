<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Muertos;
use Illuminate\Http\Request;
use App\Http\Resources\ShowResource;
use Illuminate\Support\Facades\DB;


class MuertosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $muertos = Muertos::where('fecha',$id)->first();
        //$ia14 = DB::select(DB::raw("select * from ia7 where fecha ='$id'"));
        if (!$muertos){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia7],200);
        return new ShowResource($muertos);
    }

    public function showAll()
    {

        $muertos = Muertos::all();
        if (! $muertos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$muertos],200);
    }

    public function store(Request $request)
    {
        $muertos = new Muertos();
        $muertos->fecha = $request->fecha;
        $muertos->ccaa_id = $request->ccaa_id;
        $muertos->media = $request->media;
        $muertos->save();
        return response()->json($muertos);
    }

    public function showCollection($id,$id2)
    {
        if ($id>$id2){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'Primera fecha superior a la segunda'])],404);
        }
        $muertos = DB::select(DB::raw("SELECT * FROM muertos WHERE fecha BETWEEN '$id' and '$id2'"));
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha ='$id'"));
        if (!$muertos){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new CovidCollection($muertos);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

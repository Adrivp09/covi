<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ia7 extends Model
{
    use HasFactory;
    protected $table = 'ia7';
    public $timestamps = false;
    protected $fillable = ['fecha','ccaas_id','incidencia'];
}

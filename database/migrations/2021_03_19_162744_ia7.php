<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ia7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ia7', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaa_id');
            $table->float('media');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ia7', function ($table) {
            $table->dropColumn('fecha');
            $table->dropColumn('ccaa_id');
            $table->dropColumn('media');
        });
    }
}

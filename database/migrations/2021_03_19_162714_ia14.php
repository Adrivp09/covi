<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ia14 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ia14', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaa_id');
            $table->float('media');
            $table->foreign('ccaa_id')->references('id')->on('ccaas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ia14', function ($table) {
            $table->dropColumn('fecha');
            $table->dropColumn('ccaa_id');
            $table->dropColumn('media');
        });
    }
}

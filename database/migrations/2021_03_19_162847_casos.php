<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Casos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaa_id');
            $table->integer('numero');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casos', function ($table) {
            $table->dropColumn('fecha');
            $table->dropColumn('ccaa_id');
            $table->dropColumn('media');
        });
    }
}

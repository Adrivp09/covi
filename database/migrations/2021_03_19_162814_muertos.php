<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Muertos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muertos', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaa_id');
            $table->integer('numero');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muertos', function ($table) {
            $table->dropColumn('fecha');
            $table->dropColumn('ccaa_id');
            $table->dropColumn('media');
        });
    }
}
